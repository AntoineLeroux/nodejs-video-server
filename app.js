const express = require("express");
const app = express();
const fs = require("fs");
let path = require('path')
let session = require('cookie-session');    //Express lib for session management

app.set('view engine', 'ejs')   // Template Engine

app.use('/assets', express.static('public'))    //Make assets redirect to public folder
app.disable('x-powered-by');
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(session({
    secret: 'CLE SECRETE WOW !',
    resave: false,
    saveUninitialized: true,
    proxy: true,
    cookie: {
        name: 'sessionData',
	    domain: 'videos.antoineleroux.eu',
        secure: true,
        maxAge: 4*60*60*1000 //4h
    },
}))


app.get("/", function (req, res) {
    res.sendFile(__dirname + "/pages/index.html");
});

app.get("/video/:redirect_name", function (req, res) {
    res.render(__dirname + "/pages/template", {filename_encoded:req.params.redirect_name});
});

// https://videos.antoineleroux.eu/video/NOM-FICHIER

app.get("/video", function (req, res, next) {
    // Ensure there is a range given for the video
    const range = req.headers.range;
    if(!range) {
        res.status(400).send("Requires Range header");
    }

  // get video stats (about 61MB)
  const videoPath = path.join(__dirname, './datas/',req.query.file);
    const videoSize = fs.statSync(path.join(__dirname, './datas/',req.query.file)).size;

    // Parse Range
    // Example: "bytes=32324-"
    const CHUNK_SIZE = 10 ** 6; // 1MB
    const start = Number(range.replace(/\D/g, ""));
    const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

    // Create headers
    const contentLength = end - start + 1;
    const headers = {
        "Content-Range": `bytes ${start}-${end}/${videoSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": contentLength,
        "Content-Type": "video/mp4",
    };

    // HTTP Status 206 for Partial Content
    res.writeHead(206, headers);

    // create video read stream for this particular chunk
    const videoStream = fs.createReadStream(videoPath, { start, end });

    // Stream the video chunk to the client
    videoStream.pipe(res);
});

app.listen(process.env.PORT || 55604)
